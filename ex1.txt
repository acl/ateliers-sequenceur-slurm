Write a slurm batch script that launches the script "material/script.py" on the cluster. The script
takes as command line parameters a log directory --logdir, and a float parameter --p, creates a new file
{logdir}/logs.txt, and writes {p} inside the file. Chose your own log directory as well as your own float
parameter. You must ensure that --logdir exists before launching the script (try to not ensure existence
manually).
