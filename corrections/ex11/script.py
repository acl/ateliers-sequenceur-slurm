"""Simple torch distributed example, no gpu usage."""
import os
from subprocess import check_output
from typing import Optional, Tuple

import torch
import torch.distributed as dist

def init(port: Optional[int] = None) -> Tuple[int, int]:
    rank = int(os.environ["SLURM_PROCID"])
    size = int(os.environ["SLURM_NTASKS"])
    if port is None:
        port = 10001
    raw_addrs = check_output(["scontrol", "show", "hostnames", os.environ["SLURM_JOB_NODELIST"]])
    addr = raw_addrs.split()[0].decode('utf-8')

    os.environ["RANK"] = str(rank)
    os.environ["WORLD_SIZE"] = str(size)
    os.environ["MASTER_ADDR"] = addr
    os.environ["MASTER_PORT"] = str(port)
    dist.init_process_group("nccl", init_method="env://",
                            rank=rank, world_size=size)
    return rank, size

def run(rank: int, size: int) -> None:
    print(f"Devices: {os.environ['CUDA_VISIBLE_DEVICES']}")
    device = 'cuda'
    tensor = torch.zeros(3, 3).to(device)
    group = dist.new_group([0, 1])
    if rank == 0:
        tensor.uniform_()
    dist.all_reduce(tensor, group=group)
    print(f"{rank} of {size - 1} > tensor: {tensor.tolist()}")

def main():
    rank, size = init()
    run(rank, size)


if __name__ == '__main__':
    main()
