from typing import Dict, Any, List
import argparse
from uuid import uuid4
from subprocess import call
from os.path import join
from os import makedirs
from material.utils import args2str
from itertools import product
import numpy as np
import time


def generate_args_list() -> List[Dict[str, Any]]:
    return [{'p': p} for p in np.linspace(0, 10, 100)]

def generate_batch_file(
        cmd: str, exp_name: str, logdir: str,
        cmd_args: Dict[str, Any], ngpus: int,
        ncpus: int, preview: bool=False) -> str:
    full_cmd = cmd + " " + " ".join([args2str(k, v) for k, v in cmd_args.items()])
    if preview:
        print(full_cmd)

    return open('corrections/ex7/template.slurm', 'r').read().format(
        logdir=logdir, cmd=full_cmd, ngpus=ngpus, ncpus=ncpus)

def generate_grid(var_names, args):
    prod = list(product(*args))
    return [{k:j[i] for i,k in enumerate(var_names)} for j in prod]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--exp_name", required=True, type=str)
    parser.add_argument('--p', nargs='+', help='p param', required=True)
    parser.add_argument('--q', nargs='+', help='q param', required=True)
    args = parser.parse_args()

    arg_list = generate_grid(['q', 'p'], [args.p, args.q])
    # first preview
    launch_job_list(
        'python material/script2.py',
        exp_name=args.exp_name,
        cmd_args=arg_list,
        ngpus=1,
        ncpus=1,
        narray=len(arg_list) + 1,
        preview=True)
    # then launch
    launch_job_list(
        'python material/script2.py',
        exp_name=args.exp_name,
        cmd_args=arg_list,
        ngpus=1,
        ncpus=1,
        preview=False)
