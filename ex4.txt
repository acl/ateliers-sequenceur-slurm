Bash overall lacks the flexibility and ease of use of python. We'd like to rely
as little as possible on large bash scripts to launch large scale experiments. To
that hand, write a python script run.py that runs the slurm batch script you wrote
for the previous exercise.
