The next step is to verify that you did have a GPU allocated to your job. To that end, modify
the given python script to log in the log file the GPU that was allocated to your job. Remember
that allocated GPUs can be retrieved using the CUDA_VISIBLE_DEVICES environment variable.
